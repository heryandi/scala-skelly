package dummy

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ApplicationSuite extends FunSuite {

  import Application.complicatedMethod

  test("complicatedMethod return value") {
    assert(complicatedMethod == "Hello world!")
  }
}
