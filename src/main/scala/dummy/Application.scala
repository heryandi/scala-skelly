package dummy

object Application {
  def main(args: Array[String]) = println(complicatedMethod)

  def complicatedMethod: String = "Hello world!"
}
