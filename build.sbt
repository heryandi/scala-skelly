import AssemblyKeys._

name := "Scala Skeleton Project"

scalaVersion := "2.10.2"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
    "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test",
    "junit" % "junit" % "4.11" % "test",
    "com.typesafe.akka" %% "akka-actor" % "2.1.4"
)

assemblySettings

jarName in assembly := "skelly.jar"

mainClass in assembly := Some("dummy.Application")